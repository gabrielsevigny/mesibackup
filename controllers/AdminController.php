<?php


class AdminController extends Controller {
    
    public function indexAction(){
        
        $this->validateAccess();
        
        $paramURI = $GLOBALS['Routeur']->paramURI;
        
        $view = new View();
        
        switch (@$paramURI[1]) {
            case 'exposants':
                
                //suppression d'un exposant
                if (@$paramURI[2] == 'supprimer'){
                    Exposant::getByID(@$paramURI[3])->supprimer();
                    parent::redirect('/admin/exposants');
                }
               
                //formulaire d'ajout d'un exposant
                elseif (@$paramURI[2] == 'ajouter'){   
                    echo $view->render('/admin/ajouterExposant');
                }
               
                //formulaire de modification d'un exposant
                elseif (@$paramURI[2] == 'modifier'){
                    $view->assign('exposant', Exposant::getByID($paramURI[3]));
                    echo $view->render('/admin/modifierExposant');
                }
                
               
                //Enregistre le nouvel exposant dans la BD
                elseif (@$paramURI[2] == 'enregistrer'){  
                    if (empty($_POST['hiddenID']))
                        $this->createExposant();
                    else
                        $this->saveExposant();
                    parent::redirect('/admin/exposants');
                }
                
                else {
                    $view->assign('exposants', Exposant::getAll());
                    echo $view->render('admin/gestionExposants');
                }
                
            break;
            
            case 'rapports':
                //Rapport par exposant
                if (@$paramURI[2] == 'exposants'){
                    
                    $view->assign('exposants', Exposant::getAll());
                    if (!empty($paramURI[3])){
                        
                        $exposant = Exposant::getByID($paramURI[3]);
                        $view->assign('selected', $paramURI[3]);
                        $view->assign('visiteurs', $exposant->listeVisiteurs());
                        
                        if (@$paramURI[4]=='csv'){
                            //eportation des visiteurs vers CSV
                            Export::csv($exposant->listeVisiteurs(), 'Liste_visiteurs_'.$exposant->prenom.'_'.$exposant->nom.'_'.$exposant->compagnie.'.csv', true, ';', '"' );
                        }
                        
                    } else {
                        $view->assign('selected', null);
                        $view->assign('visiteurs', array());
                    }
                    echo $view->render('admin/rapportExposants');
                    
                }
                //Rapport par Utilisatuer
                elseif (@$paramURI[2] == 'utilisateurs'){
                    
                    if (@$paramURI[3]=='csv'){
                        //eportation des visiteurs vers CSV
                        Export::csv(User::getAll(), 'Liste_visiteurs_complete.csv', true, ';', '"' );
                    } else {
                        $view->assign('utilisateurs', User::getAll());
                        echo $view->render('admin/rapportUtilisateurs');
                    }
                }
                
            break;
           
            case 'logout':
                unset($_SESSION['admp']);
                parent::redirect('/');
            break;
        
            case 'resetusers':
                if (@$paramURI[2] == 'confirm'){
                    foreach (User::getAll() as $user){
                        $user->supprimer();
                    }
                    parent::redirect('/admin');
                }
                echo $view->render('admin/resetusers');
            break;
        
            default:
                echo $view->render('admin/admin');
        }
        
        
        
    }
    
    /**
     * Crée un exposant dans la BD à partir du formulaire web
     */
    private function createExposant(){
        
        //Crée l'Exposant
        $exposant = new Exposant();
        
        if (!empty($_FILES['filePDF']['tmp_name'])){
            //Copie le fichier PDF  
            $filePath = '/assets/pdf/' . time() . rand(10, 99) . '_' . $_FILES['filePDF']['name'];
            move_uploaded_file($_FILES['filePDF']['tmp_name'], Config::appPath . $filePath);
            $exposant->pdfPath = $filePath;
        }
        
        
        $exposant->nom = $_POST['txtNom'];
        $exposant->prenom = $_POST['txtPrenom'];
        $exposant->compagnie = $_POST['txtCompagnie'];
        $exposant->email = $_POST['txtEmail'];
        $exposant->telephone = $_POST['txtTelephone'];
        $exposant->save();
    }
    
    /**
     * Modifie un exposant dans la BD à partir du formulaire web
     */
    private function saveExposant(){
        
        $exposant = Exposant::getByID($_POST['hiddenID']);
        
        if (!empty($_POST['chkDelFile'])){
            $exposant->pdfPath = '';
        }
        
        if (!empty($_FILES['filePDF']['tmp_name'])){
            //Copie le fichier PDF
            $filePath = '/assets/pdf/' . time() . rand(10, 99) . '_' . $_FILES['filePDF']['name'];
            move_uploaded_file($_FILES['filePDF']['tmp_name'], Config::appPath . $filePath);
            $exposant->pdfPath = $filePath;
        }
        
        $exposant->nom = $_POST['txtNom'];
        $exposant->prenom = $_POST['txtPrenom'];
        $exposant->compagnie = $_POST['txtCompagnie'];
        $exposant->email = $_POST['txtEmail'];
        $exposant->telephone = $_POST['txtTelephone'];
        
        $exposant->save();
    }
    
    /**
     * Valide si l'utilisateur à accès à la section admin. Sinon affiche le formulaire de login.
     */
    public function validateAccess() {
        
        if (isset($_POST['admp'])){
            $_SESSION['admp'] = sha1($_POST['admp']);
        }
        
        if (@$_SESSION['admp'] != sha1(Config::AdminPassword)){
            $view = new View();
            echo $view->render('admin/adminLogin');
            exit();
        }
        
    }
    
}
