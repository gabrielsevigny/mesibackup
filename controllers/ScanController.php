<?php


class ScanController extends Controller {
    
    public function indexAction(){
        $paramURI = $GLOBALS['Routeur']->paramURI;
        
        $view = new View();
        
        //Récupère l'exposant
        $Exposant = Exposant::getByHash($paramURI[1]);
        
        //Enregistre la visite
        try{
            Visite::create($GLOBALS['Session']->User->id, $Exposant->id);
        } catch (Exception $e) {}
        
        if ($Exposant instanceof Exposant){
            $view->assign('exposant', $Exposant);
            $view->assign('success', true);
        } else {
            $view->assign('success', false);
        }
        echo $view->render('scan');
    }
    
}
