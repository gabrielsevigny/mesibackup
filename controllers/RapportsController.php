<?php

class RapportsController extends Controller {
    
    public function indexAction(){
        
        $paramURI = $GLOBALS['Routeur']->paramURI;
        $view = new View;
        
        switch (@$paramURI[1]){
            
            case 'exposants':
                $view->assign('exposants', Exposant::getAll());
                echo $view->render('exposants');
                break;
            
            case 'mesvisites':
            default:
                echo $view->render('mesVisites');
                break;
            
        }
    }
    
}
