<?php


class HttpErrorController extends Controller{
    
    public function show404(){
        header("HTTP/1.0 404 Not Found");
        $view = new View();
        echo $view->render('404');
    }
    
}
