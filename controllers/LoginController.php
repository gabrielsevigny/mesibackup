<?php


class LoginController extends Controller {
 
    
    public function indexAction(){
        
        $paramURI = $GLOBALS['Routeur']->paramURI;
        $view = new View();
        
        switch (@$paramURI[1]){
            
            case 'consentement':
                echo $view->render('consentement');
            break;
        
            case 'accepter':
                $this->accpeterCondidition();
                parent::redirect('/home');
            break;
            
            case 'auth':
                $this->logUser();
                break;
            
            case 'logout':
                $this->logOut();
                break;
            
            default:
                //Si l'utilisatuer n'a pas accepté les conditions,
                if (!$this->conditionsAcceptees()){
                    parent::redirect('/login/consentement');
                }
                
                //Affiche le formulaire de login
                if (!empty($_GET['erreur'])){
                    $view->assign('errorMessage', '1');
                }
                echo $view->render('login');
        }
    }
    
    
    /**
     * Si l'utilisateur n'est pas loggué, on redirige vers la page de login
     */
    public function validerSession() {
        if (!$GLOBALS['Session']->isLoggued() && ($GLOBALS['Routeur']->paramURI[0] != 'login')){
            parent::redirect('/login');
        }
    }
    
    /**
     * Loggue le user en utilisant le $_POST
     * Si le courriel n'existe pas dans la BD, un nouveau user est créé
     * Si tous les champs ne sont pas complété, redirige vers la page de login
     */
    public function logUser($redirect = "/home"){
        
        //on valide le formulaire
        if (empty($_POST['txtNom']) || !filter_var($_POST['txtEmail'],FILTER_VALIDATE_EMAIL)) {
            parent::redirect('/login?erreur=1');
        }
        
        //On récupère le user dans la BD
        $user = User::getByEmail($_POST['txtEmail']);
        
        //Si le user n'existe pas, on le crée dans la BD
        if (!$user instanceof User){
            $user = new User;
            $user->nom = $_POST['txtNom'];
            $user->compagnie = $_POST['txtCompagnie'];
            $user->email = $_POST['txtEmail'];
            $user->telephone = $_POST['txtTelephone'];
            $user->save();
        }
        
        //On log le user dans la session
        $GLOBALS['Session']->logIn($user);
        
        //Redirige vers la bonne page
        parent::redirect($redirect);
    }
    
    public function logOut(){
        $GLOBALS['Session']->logOut();
        parent::redirect();
    }
    
    /**
     * Confime que l'utilisateur accepte les conditions
     */
    public function accpeterCondidition(){
        setcookie("conditions", true, time()+3600*24*365, '/'); 
    }
    
    /**
     * Indique si l'utilisateur a bien accepté les conditions
     * @return bool
     */
    public function conditionsAcceptees(){
        return !empty($_COOKIE['conditions']);
    }
    
}
