<?php


class HomeController extends Controller {
    
    public function indexAction(){
        
        $paramURI = $GLOBALS['Routeur']->paramURI;
        $view = new View();
        
        switch (@$paramURI[1]){
            
            case 'splash':
                echo $view->render('splash');
            break;
            
            default:            
                echo $view->render('home');
            
        }
        
    }
    
}
