<?php

/*
 * Fichier de config
 */

class Config {

    const dbHost = 'localhost';
    const dbName = 'c1_qr';
    const dbUser = 'c0_qr';
    const dbPass = 'algego1234!';

    const debug = false;
    const forceSSL = false;

    const appTitle = 'QR';
    
    const appURL = '/';
    const appPath = __DIR__;
    
    const AdminPassword = 'qr12345';
}