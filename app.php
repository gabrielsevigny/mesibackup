<?php

session_start();

require 'Config.php';


//Si le mode débug est activé
if (Config::debug == true){
    //on affiche toutes les erreurs
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

/*
 * Inclusion des classes 
 */

require Config::appPath.'/src/Database.php';
require Config::appPath.'/src/Router.php';
require Config::appPath.'/src/View.php';
require Config::appPath.'/src/Model.php';
require Config::appPath.'/src/Controller.php';
require Config::appPath.'/src/Session.php';
require Config::appPath.'/src/Export.php';

require Config::appPath.'/models/User.php';
require Config::appPath.'/models/Exposant.php';
require Config::appPath.'/models/Visite.php';

require Config::appPath.'/controllers/HomeController.php';
require Config::appPath.'/controllers/LoginController.php';
require Config::appPath.'/controllers/HttpErrorController.php';
require Config::appPath.'/controllers/RapportsController.php';
require Config::appPath.'/controllers/AdminController.php';
require Config::appPath.'/controllers/ScanController.php';

/**
 * Crée l'objet de session
 */
$Session = new Session();

/**
 * Apelle le controlleur principal (routeur)
 */
$Routeur = new Router();
$Routeur->indexAction();