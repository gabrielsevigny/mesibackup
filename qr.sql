-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 24 Mai 2018 à 15:33
-- Version du serveur :  5.5.52-MariaDB
-- Version de PHP :  5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `c1_qr`
--

-- --------------------------------------------------------

--
-- Structure de la table `exposant`
--

CREATE TABLE IF NOT EXISTS `exposant` (
  `id` int(10) unsigned NOT NULL,
  `hashId` varchar(255) NOT NULL,
  `compagnie` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(255) NOT NULL,
  `pdfPath` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `exposant`
--

INSERT INTO `exposant` (`id`, `hashId`, `compagnie`, `nom`, `prenom`, `email`, `telephone`, `pdfPath`) VALUES
(25, '4196041389', 'AVIR Pharma Inc.', 'Kadri', 'Kaled ', 'kkadri@avirpharma.com', '', ''),
(26, '1662243607', 'AVIR Pharma Inc.', ' Fortin', 'Vanessa', 'vfortin@avirpharma.com', '', ''),
(27, '336913281', 'BioAuxilium', ' Padros', 'Jaime', '''jaime.padros@bioauxilium.com', '514-668-7235', ''),
(28, '2225864208', 'BIOGENIQ', 'Crevier', 'Etienne ', 'ETIENNE.CREVIER@BIOGENIQ.CA', '514.952.6798', ''),
(30, '2473281379', 'BIOGENIQ', 'Bégin', 'Antoine', 'antoine.begin@biogeniq.ca', '', ''),
(35, '3808539628', 'COREALIS Pharma Inc.', ' Roy', 'Yves', 'yves.roy@corealispharma.com', '514-889-9304', ''),
(36, '2047402582', 'Feldan Therapeutics ', 'Michaud', 'François-Thomas ', 'ftmichaud@feldan.com', '418.953.3048', ''),
(37, '219140800', 'Feldan Therapeutics ', ' Ménard', 'Vincent', 'vmenard@feldan.com', '418.446.1529', ''),
(38, '2645610321', 'Hexoskin', 'Trinh', 'Thu Ngan ', 'ntn_trinh@hexoskin.com', '514-266-7285', ''),
(39, '3937927111', 'Hexoskin', 'Paquin', 'Marc ', 'marc.paquin@hexoskin.com', '514-602-1846', ''),
(40, '3693793700', 'Imeka', 'Bélanger', 'Jean-René ', 'jr.belanger@imeka.ca', '1-819-238-2602', ''),
(41, '2871910706', 'Imeka', ' Mistry', 'Helena', 'helena.mistry@imeka.ca', '1-617-953-1346', ''),
(42, '841265288', 'Immune Biosolutions', 'Robitaille', 'Yolaine ', 'yrobitaille@ibiosolutions.com', '514-830-0963', ''),
(43, '1159954462', '', 'Gaudreau', 'Simon ', 'sgaudreau@ibiosolutions.com', '819 679-0718', '');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `compagnie` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `email`, `nom`, `telephone`, `compagnie`) VALUES
(30, 'info555@kevinbelanger.com', 'Kevin Bélanger', '4188091448', 'kb consults.'),
(31, 'marieve_l@hotmail.com', 'm-eve langlois', '', 'dfg');

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

CREATE TABLE IF NOT EXISTS `visite` (
  `userId` int(10) unsigned NOT NULL,
  `exposantId` int(10) unsigned NOT NULL,
  `ts` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `visite`
--

INSERT INTO `visite` (`userId`, `exposantId`, `ts`) VALUES
(15, 30, 1527091116),
(31, 30, 1527128006);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `exposant`
--
ALTER TABLE `exposant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `visite`
--
ALTER TABLE `visite`
  ADD PRIMARY KEY (`userId`,`exposantId`),
  ADD KEY `timestmp` (`ts`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `exposant`
--
ALTER TABLE `exposant`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
