<?php


class User extends Model {
    
    public $id;
    public $email;
    public $nom;
    public $telephone;
    public $compagnie;
    
    /**
     * Retourne un user à partir de son ID
     * Si aucun user n'est trouvé, retourne null
     * @param int $id
     * @return User
     */
    public static function getByID($id) {
        
        $req = Database::getPDO()->prepare('SELECT * FROM user WHERE id=:id');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->execute();
        
        if (!$req->rowCount())
            return null;
        
        return $req->fetchObject('User');
    }
    
    /**
     * Retourne un user à partir de son email
     * Si aucun user n'est trouvé, retourne null
     * @param string $email
     * @return User
     */
    public static function getByEmail($email) {
        
        $req = Database::getPDO()->prepare('SELECT * FROM user WHERE email=:email');
        $req->bindValue(':email', $email, PDO::PARAM_STR);
        $req->execute();
        
        if (!$req->rowCount())
            return null;
        
        return $req->fetchObject('User');
        
    }
    
    /**
     * Retourne un array de tous les utilisatuer de la BD
     * @return User
     */
    public static function getAll(){
        
        $req = Database::getPDO()->query('SELECT * FROM user');
        
        if (!$req->rowCount())
            return array();
        
        return $req->fetchAll(PDO::FETCH_CLASS,'User');
        
    }
    
    public function nbVisite(){
        $req = Database::getPDO()->prepare('SELECT count(*) as nb FROM visite WHERE userId=:userId');
        $req->bindValue(':userId', $this->id, PDO::PARAM_INT);
        $req->execute();
        
        if (!$req->rowCount())
            return 0;
        
        return $req->fetch()['nb'];
    }
    
    public function listExposants(){
        $req = Database::getPDO()->prepare('SELECT exposant.* FROM exposant INNER JOIN visite ON exposant.id = visite.exposantId WHERE visite.userId = :userId ORDER BY compagnie');
        $req->bindValue(':userId', $this->id, PDO::PARAM_INT);
        $req->execute();
        
        return $req->fetchAll(PDO::FETCH_CLASS,'Exposant');
    }
    
    
    public function save(){
        
        if (!empty($this->id)){
            $req = Database::getPDO()->prepare('UPDATE user SET nom=:nom, compagnie=:compagnie, email=:email, telephone=:telephone WHERE id=:id');
            $req->bindValue(':nom', $this->nom, PDO::PARAM_STR);
            $req->bindValue(':compagnie', $this->compagnie, PDO::PARAM_STR);
            $req->bindValue(':email', $this->email, PDO::PARAM_STR);
            $req->bindValue(':telephone', $this->telephone, PDO::PARAM_STR);
            $req->bindValue(':id', $this->id, PDO::PARAM_STR);
            $req->execute();
            
        } else {
            $req = Database::getPDO()->prepare('INSERT INTO user (nom, compagnie, email, telephone) VALUES (:nom, :compagnie, :email, :telephone)');
            $req->bindValue(':nom', $this->nom, PDO::PARAM_STR);
            $req->bindValue(':compagnie', $this->compagnie, PDO::PARAM_STR);
            $req->bindValue(':email', $this->email, PDO::PARAM_STR);
            $req->bindValue(':telephone', $this->telephone, PDO::PARAM_STR);
            $req->execute();
            $this->id = Database::getPDO()->lastInsertId();
        }
        
    }
    
    public function supprimer(){
        $req = Database::getPDO()->prepare('DELETE FROM user WHERE id = :id');
        $req->bindValue(':id', $this->id, PDO::PARAM_INT);
        $req->execute();
        
        $req = Database::getPDO()->prepare('DELETE FROM visite WHERE userId = :id');
        $req->bindValue(':id', $this->id, PDO::PARAM_INT);
        $req->execute();
    }
    
}
