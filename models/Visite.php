<?php

Class Visite extends Model {
    
    public $user;
    public $exposant;
    public $timestamp;
    
    public static function create($userID, $exposantId){
        $req = Database::getPDO()->prepare('INSERT INTO visite (userId, exposantID, ts) VALUES (:userId, :exposantID, :ts);');
        $req->bindValue(':userId', $userID, PDO::PARAM_INT);
        $req->bindValue(':exposantID', $exposantId, PDO::PARAM_INT);
        $req->bindValue(':ts', time(), PDO::PARAM_INT);
        $req->execute();
    }
    
}