<?php


class Exposant extends Model {
    
    public $id;
    public $hashid;
    public $compagnie;
    public $nom;
    public $prenom;
    public $email;
    public $telephone;
    public $pdfPath;
    
    /**
     * Retourne un user à partir de son ID
     * @param int $id
     * @return Exposant
     */
    public static function getByID($id) {
        
        $req = Database::getPDO()->prepare('SELECT * FROM exposant WHERE id=:id');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->execute();
        
        if (!$req->rowCount())
            return null;
        
        return $req->fetchObject('Exposant');
    }

    /**
     * Retourne un user à partir de son email
     * @param string $email
     * @return User
     */
    public static function getByHash($hash) {
        
        $req = Database::getPDO()->prepare('SELECT * FROM exposant WHERE hashId=:hash');
        $req->bindValue(':hash', $hash, PDO::PARAM_STR);
        $req->execute();
        
        if (!$req->rowCount())
            return null;
        
        return $req->fetchObject('Exposant');
        
    }
    
    /**
     * Retourne un array de tous les exposants
     * @return Exposant
     */
    public static function getAll(){
        
        $req = Database::getPDO()->query('SELECT * FROM exposant ORDER BY compagnie');
        return $req->fetchAll(PDO::FETCH_CLASS, 'Exposant');
        
    }
    
    /**
     * Retourne un tableau contennt tous les users qui ont scanné l'exposant
     * @return User array de User
     */
    public function listeVisiteurs() {
        $req = Database::getPDO()->prepare('SELECT user.* FROM user INNER JOIN visite on user.Id = visite.UserId WHERE visite.ExposantID = :id;');
        $req->bindValue(':id', $this->id, PDO::PARAM_STR);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_CLASS,'User');
    }
  
    
    public function save(){
        
        if ($this->pdfPath===NULL)
            $this->pdfPath = '';
        
        if (!empty($this->id)){
            $req = Database::getPDO()->prepare('UPDATE exposant SET compagnie=:compagnie, nom=:nom, prenom=:prenom, email=:email, telephone=:telephone, pdfPath=:pdfPath WHERE id=:id');
            $req->bindValue(':compagnie', $this->compagnie, PDO::PARAM_STR);
            $req->bindValue(':nom', $this->nom, PDO::PARAM_STR);
            $req->bindValue(':prenom', $this->prenom, PDO::PARAM_STR);
            $req->bindValue(':email', $this->email, PDO::PARAM_STR);
            $req->bindValue(':telephone', $this->telephone, PDO::PARAM_STR);
            $req->bindValue(':pdfPath', $this->pdfPath, PDO::PARAM_STR);
            $req->bindValue(':id', $this->id, PDO::PARAM_STR);
            $req->execute();
            
        } else {
            $req = Database::getPDO()->prepare('INSERT INTO exposant (compagnie, nom, prenom, email, telephone, pdfPath) VALUES (:compagnie, :nom, :prenom, :email, :telephone, :pdfPath)');
            $req->bindValue(':compagnie', $this->compagnie, PDO::PARAM_STR);
            $req->bindValue(':nom', $this->nom, PDO::PARAM_STR);
            $req->bindValue(':prenom', $this->prenom, PDO::PARAM_STR);
            $req->bindValue(':email', $this->email, PDO::PARAM_STR);
            $req->bindValue(':telephone', $this->telephone, PDO::PARAM_STR);
            $req->bindValue(':pdfPath', $this->pdfPath, PDO::PARAM_STR);
            $req->execute();
            $this->id = Database::getPDO()->lastInsertId();
            $this->setHashId();
        }
        
    }
    
    private function setHashId(){
        $hash = crc32($this->id);
        $req = Database::getPDO()->prepare('UPDATE exposant SET hashId = :hashID WHERE id=:id');
        $req->bindValue(':hashID', $hash, PDO::PARAM_STR);
        $req->bindValue(':id', $this->id, PDO::PARAM_INT);
        $req->execute();
        $this->hashid = $hash;
    }
    
    public function supprimer(){
        $req = Database::getPDO()->prepare('DELETE FROM exposant WHERE id = :id');
        $req->bindValue(':id', $this->id, PDO::PARAM_INT);
        $req->execute();
        
        $req = Database::getPDO()->prepare('DELETE FROM visite WHERE exposantId = :id');
        $req->bindValue(':id', $this->id, PDO::PARAM_INT);
        $req->execute();
    }
    
}
