<?php


class Database {
    
    private static $PDO;
    
    /**
     * @return PDO
     */
    public static function getPDO(){
        
        if (!(self::$PDO instanceof PDO)){
            self::$PDO = new PDO('mysql:host='.Config::dbHost.';dbname='.Config::dbName.';charset=utf8', Config::dbUser, Config::dbPass);
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$PDO->exec('SET NAMES UTF8');
        }
        
        return self::$PDO;
    }
    
}   
