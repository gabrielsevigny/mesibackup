<?php

class View {

    protected $data;

    function render($template) {
        //$data est accessible dans le template
        $data = $this->data;
        //$Session est accessible dans le template
        global $Session;
        
        ob_start();
        require Config::appPath . "/templates/$template.php";
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

    function assign($key, $val) {
        $this->data[$key] = $val;
    }
}