<?php

class Router {
    
    public $path;
    public $paramURI;
    

    public function __construct() {
        //Parse le path
        $this->path = @$_GET['path'];
        $this->paramURI = explode('/', $this->path);
    }
    

    public function indexAction(){
        
        //On force la connexion ssl au besoin
        if(Config::forceSSL && @$_SERVER["HTTPS"] != "on") {
            header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
            exit();
        }
        
        //page par défaut: splash screen
        if (empty($this->paramURI[0])){
            $this->paramURI[0]='home';
            $this->paramURI[1]='splash';
        } elseif($this->paramURI[0]!='admin') {
            //à l'exception du splash screen et de la zone admin
            //Valide la session et redirige vers la page de login au besoin
            $Login = new LoginController();
            $Login->validerSession();
            
        }
        
        
        
        //Appel le bon controleur en se basant sur le path
        $className = $this->paramURI[0].'Controller';
        
        if (method_exists($className,'indexAction')){
            $controller = new $className();
            $controller->indexAction();
        } else {
            $controller = new HttpErrorController();
            $controller->show404();
        }
        
        
    }
    
}
