<?php

class Controller {

    /**
     * User actuellement authentifié
     * Null si non authentifié
     * @var User
     */
    public static $User;

    /**
     * Permet de rediriger vers un URL
     * @param string $url
     */
    public static function redirect($url=""){
        if (empty($url)) {
            $url = Config::appURL;
        }
        header("location: {$url}");
        exit();
    }
    
}