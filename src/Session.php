<?php
/**
 * Représente la session courrante
 */
class Session {
       
    /**
     * User actuellement authentifié
     * Null si non authentifié
     * @var User
     */
    public $User;
    
    
    public function __construct() {
        //Log à partir du cookie si possible
        if (isset($_COOKIE['uid'])){
            $this->User = User::getByID($_COOKIE['uid']);
        }
    }
    
    /**
     * Indique si un utilisateur est loggué
     * @return bool
     */
    public function isLoggued(){
        return ($this->User instanceof User);
    }
    
    /**
     * Supprime le cookie de session et retire le user loggué
     */
    public function logOut(){
        $this->User = null;
        setcookie('uid', null, -1, '/');
    }
    
    /**
     * Créé le cookkie de session et 
     * @param User $uid
     */
    public function logIn($User){
        setcookie("uid", $User->id, time()+3600*24*365, '/'); 
        $this->User  = $User;
    }
}
