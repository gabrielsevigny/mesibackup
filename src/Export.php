<?php

/**
 * Helper pour exporter du data vers des fichier
 *
 * @author Kev
 */
class Export {
    public static function csv(array $data, $filename = 'data.csv', $header = false, $delimiter = ',', $quote = ''){
        
        header('Content-Type: text/csv; charset=ISO-8859-1');
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $output = '';
        
        if ($header) {
            array_unshift($data, array_keys((array)$data[0]));
        }
        
        foreach ($data as $line){
            $line = (array) $line;
            foreach($line as $item) {
                $output .= $quote . mb_convert_encoding($item, 'ISO-8859-1') . $quote . $delimiter;
            }
            $output = trim($output, $delimiter) . "\r\n";
        }
        echo $output;
        exit();
    }
}
