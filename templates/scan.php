<?php
/*
 * Paramètres:
 *
 * Indique si le scan à réussi
 * $data['success']
 *
 * Objet exposant correspodant au code scanné
 * $data['exposant']
 *
 */
?>

<?php include 'header.php'; ?>


<?php if ( $data['success'] ): ?>
    <div class="mainTitle">
        <h1>Vous avez rencontré :<br>
            <span class="english">You have met :</span></h1>
    </div>

    <p class="borderContent">
        <b><?php echo $data['exposant']->prenom; ?><?php echo $data['exposant']->nom; ?></b><br>
		<?php echo $data['exposant']->compagnie; ?><br>
		<?php echo $data['exposant']->email; ?><br>
    </p>
    <p>Ses coordonnées ont bien été enregistrées<br><span class="spacer spacer-5"></span>
        <span class="greenTypo">His coordinates have been recorded</span></p>
<?php else: ?>
    <div class="msg msg--alert">
        <p>Il nous a été impossible d'identifier l'exposant à partir du code QR. Veuillez réessayer.</p>
        <hr>
        <p><i>We were unable to identify the exhibitor from the QR code. Try again.</i></p>
    </div>
<?php endif; ?>
    <span class="spacer spacer-5"></span>
    <p><a class="btn btn-green waves-light waves-effect" href="/home">Continuer<br>Continue</a></p>

<?php include 'footer.php'; ?>