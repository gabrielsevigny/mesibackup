<html>
<head>
    <title><?php echo Config::appTitle; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/normalize.css">
    <link rel="stylesheet" href="/assets/styles.css">
    <script src="/assets/jQuery.js" type="text/javascript"></script>
    <script src="/assets/qr.js" type="text/javascript"></script>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTable').DataTable(
                {
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json"
                    }
                }
            );
        });
    </script>
    <style>
        .mobileOnlyMsg{
            display: none;
        }
    </style>
</head>
<body>

<div id="bodyWrapper">

    <header class="banner__top">
        <div class="banner__top-nav">
            <button class="backNav waves-effect waves-light" onclick='window.location.href="/admin"'><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <div class="siteTitle">Bio2018 - Admin</div>
            <button class="closeAdmin waves-effect waves-light" onclick="window.location.href='/admin/logout'"><i class="fa fa-power-off" aria-hidden="true"></i>
            </button>
        </div>
        <img src="/assets/img/bannertop.png" alt="">
    </header>
    <div class="bodyWrapper-content">