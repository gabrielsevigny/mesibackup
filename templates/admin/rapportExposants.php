<?php
/*
 * Rapport de toutes les visites par exposants
 * 
 * paramètres:
 * $data['exposants'] Contient la liste de tous les exposantss (array d'objet Exposant)
 * $data['visiteurs'] Conteient la liste des utilisateurs qui on scanné l'exposant sélectionné ou un array vide si aucun exposant n'est sélectionner
 * $data['selected'] Contient l'ID de l'exposant sélectionné
 */
?>

<?php include 'headerAdmin.php'; ?>

<div class="mainTitle">
    <h1>Rapport par exposant</h1>
</div>
<span class="spacer spacer-30"></span>
<select id="lstExopsants">
    <?php foreach ($data['exposants'] as $exposant): ?>
        <option <?php echo ($exposant->id == $data['selected'])?'selected':'' ?> value="<?php echo $exposant->id;?>"><?php echo $exposant->prenom . ' ' . $exposant->nom . ' (' . $exposant->compagnie . ')' ;?></option>
    <?php endforeach; ?>
</select><br>
<a href="<?php echo $data['selected'] ?>/csv">Exporter en CSV</a>
<script>
    $('#lstExopsants').change(function(){
        window.location.href = '/admin/rapports/exposants/' + $(this).val();
    });
</script>

<table class="dataTable">
    <thead>
        <tr>
            <th>Nom complet</th>
            <th>Compagnie</th>
            <th>Courriel</th>
            <th>Téléphone</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['visiteurs'] as $visiteur): ?>
            <tr>
                <td><?php echo $visiteur->nom ?></td>
                <td><?php echo $visiteur->compagnie ?></td>
                <td><?php echo $visiteur->email ?></td>
                <td><?php echo $visiteur->telephone ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php include 'footerAdmin.php'; ?>

