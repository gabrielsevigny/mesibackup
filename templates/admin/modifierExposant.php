<?php
/*
 * Gestion des exposants.
 * à partir de cette page les admin peuvent afficher la liste des exposants
 * ajouter des exposants
 * supprimer des exposants
 * 
 * paramètres:
 * $data['exposant'] Contient l'exposant à modifier
 */
?>
<?php include 'headerAdmin.php'; ?>

<div class="addParticipant">
    <h1>Ajouter un participant</h1>

    <form method="post" enctype="multipart/form-data" action="/admin/exposants/enregistrer">
        <input type="hidden"  value="<?php echo $data['exposant']->id ?>" name="hiddenID">
        <input type="text" name="txtPrenom" placeholder="Prénom" value="<?php echo $data['exposant']->prenom ?>"/>
        <input type="text" name="txtNom" placeholder="Nom"  value="<?php echo $data['exposant']->nom ?>" />
        <input type="email" name="txtEmail" placeholder="Courriel" value="<?php echo $data['exposant']->email ?>" />
        <input type="text" name="txtTelephone" placeholder="Telephone" value="<?php echo $data['exposant']->telephone ?>" />
        <input type="text" name="txtCompagnie" placeholder="Compagnie" value="<?php echo $data['exposant']->compagnie ?>" />
        Supprimer le fichier: <input type="checkbox" name="chkDelFile">
        Remplacer le fichier: <input type="file" name="filePDF" id="fileToUpload">
        <input class="btn btn-green" type="submit" name="cmdAjoutExposant"><br>
        <a class="btn btn-gray" href="/admin/exposants">Annuler</a>
    </form>
</div>


<?php include 'footerAdmin.php'; ?>

