<?php include 'headerAdmin.php'; ?>

<div class="login">
    <div class="mainTitle">
        <h1>Zone sécurisée</h1>
        <h1 class="english">Security Zone</h1>
    </div>
    <p>Veuillez entrer le mot de passe administrateur <span class="spacer spacer-5"></span>
        <span class="greenTypo">Please enter the administrator password</span></p>
    <form action="/admin" method="post">
        <input type="password" name="admp" />
        <br>
        <input class="btn btn-green" type="submit" value="VALIDER / SUBMIT"/>
    </form>
    <span class="spacer spacer-15"></span>
    <a href="/home"><span class="blueTypo">Retourner à l'accueil</span><span class="spacer spacer-5"></span>Return Home</a>
</div>

<?php include 'footerAdmin.php'; ?>