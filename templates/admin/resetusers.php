<?php include 'headerAdmin.php'; ?>

    <div class="mainTitle">
        <h1>Gestion de l'application</h1>
        <h1 class="english">Application Management</h1>
    </div>
    <span class="spacer spacer-15"></span>
    <h3>Mise en garde:</h3>
    <p>Si vous continuez, tous les utilsatuers seront supprimés définitivement. Ceci ne supprime pas les exposants.</p>
    <h3>Êtes-vous certain de vouloir supprimer tous les utilisateurs qui se sont connecté à l'application?</h3>
    <div class="twoBtnHalf">
        <div class="btnHalf">
            <a onclick="return confirm('Êtes-vous absolument certain?')" class='btn btn-green' style='background-color: red;' href="/admin/resetusers/confirm">OUI</a>
        </div>
        <div class="btnHalf">
            <a class='btn btn-green' href="/admin">NON</a>
        </div>
    </div>
<?php include 'footerAdmin.php'; ?>