<?php
/*
 * Gestion des exposants.
 * à partir de cette page les admin peuvent afficher la liste des exposants
 * ajouter des exposants
 * supprimer des exposants
 * 
 * paramètres:
 * $data['exposants'] Contient la liste de tous les exposantss (array d'objet Exposant)
 */
?>
<?php include 'headerAdmin.php'; ?>

<div class="addParticipant">
    <h1>Ajouter un exposant</h1>
    <form method="post" enctype="multipart/form-data" action="/admin/exposants/enregistrer">
        <input type="text" name="txtPrenom" placeholder="Prénom"/>
        <input type="text" name="txtNom" placeholder="Nom" />
        <input type="email" name="txtEmail" placeholder="Courriel" />
        <input type="text" name="txtTelephone" placeholder="Télephone" />
        <input type="text" name="txtCompagnie" placeholder="Compagnie" />
        <input type="file" name="filePDF" id="fileToUpload">
        <input class="btn btn-green" type="submit" name="cmdAjoutExposant"><br>
        <a class="btn btn-gray" href="/admin/exposants">Annuler</a>
    </form>
</div>


<?php include 'footerAdmin.php'; ?>

