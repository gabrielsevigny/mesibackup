<?php
/*
 * Rapport du nombre de visite pas utilisateurs
 * 
 * paramètres:
 * $data['utilisateurs'] Contient la liste de tous les utilisatuers (array d'objet Utilisateur)
 */
?>

<?php include 'headerAdmin.php'; ?>

<!--
ToDo
Voir les informations label
-->
<div class="mainTitle">
    <h1>Rapport par utilisateur</h1>
    <a href="/admin/rapports/utilisateurs/csv">Exporter en CSV</a>
</div>
<table class="dataTable">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Compagnie</th>
            <th>Courriel</th>
            <th>Téléphone</th>
            <th>Nb visite</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['utilisateurs'] as $visiteur): ?>
            <tr>
                <td><?php echo $visiteur->nom ?></td>
                <td><?php echo $visiteur->compagnie ?></td>
                <td><a href="mailto:<?php echo $visiteur->email ?>"><?php echo $visiteur->email ?></a></td>
                <td><?php echo $visiteur->telephone ?></td>
                <td><?php echo $visiteur->nbVisite() ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php include 'footerAdmin.php'; ?>

