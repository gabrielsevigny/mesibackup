<?php include 'headerAdmin.php'; ?>

    <div class="mainTitle">
        <h1>Gestion de l'application</h1>
        <h1 class="english">Application Management</h1>
    </div>
    <span class="spacer spacer-15"></span>
    <ul class="liste--exposants">
        <li><a class="btn btn-green waves-effect waves-light" href="/admin/exposants">Gérer la liste des exposants</a><br></li>
        <li><a class="btn btn-green waves-effect waves-light" href="/admin/rapports/utilisateurs">Rapport détaillé par utilisateur</a><br></li>
        <li><a class="btn btn-green waves-effect waves-light" href="/admin/rapports/exposants">Rapport détaillé par exposant</a></li>
        <li><a class="btn btn-gray waves-effect waves-light btnDisable" href="/admin/resetusers">Supprimer tous les utilisateurs et les visites</a><br></li>
    </ul>


<?php include 'footerAdmin.php'; ?>