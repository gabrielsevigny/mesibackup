<?php
/*
 * Gestion des exposants.
 * à partir de cette page les admin peuvent afficher la liste des exposants
 * ajouter des exposants
 * supprimer des exposants
 * 
 * paramètres:
 * $data['exposants'] Contient la liste de tous les exposantss (array d'objet Exposant)
 */
?>

<?php include 'headerAdmin.php'; ?>
<!--ToDo
mettre les exposants à jour
-->
<div class="mainTitle">
    <h1>Gestion des exposants</h1>
</div>
<span class="spacer spacer-30"></span>
<a class="btn btn-green waves-effect waves-light" href="/admin/exposants/ajouter">Ajouter un exposant</a>

<table class="datatable">
    <thead>
    <tr>
        <th>Prenom</th>
        <th>Nom</th>
        <th>Compagnie</th>
        <th>Courriel</th>
        <th>Téléphone</th>
        <th>PDF</th>
        <th>...</th>
    </tr>
    </thead>
	<?php foreach ( $data['exposants'] as $exposant ): ?>
        <tr class=" gestionExposant">
            <td><?php echo $exposant->prenom ?></td>
            <td><?php echo $exposant->nom ?></td>
            <td><?php echo $exposant->compagnie ?></td>
            <td><a href="mailto:<?php echo $exposant->email ?>"><?php echo $exposant->email ?></a></td>
            <td><?php echo $exposant->telephone ?></td>
            <td><a href="<?php echo $exposant->pdfPath ?>"><?php echo $exposant->pdfPath ?></a></td>
            <td>
                <div class="btnAdmin">
                    <div class="btnAdminThird">
                        <a title="Supprimer"
                           onclick="return confirm('Voulez-vous supprimer l\'exposant <?php echo $exposant->prenom . ' ' . $exposant->nom; ?> ?')"
                           href="/admin/exposants/supprimer/<?php echo $exposant->id; ?>"><i
                                    class="fa fa-trash"></i></a>
                    </div>
                    <div class="btnAdminThird">
                        <a title="Modifier" href="/admin/exposants/modifier/<?php echo $exposant->id ?>"><i
                                    class="fa fa-edit"></i></a>
                    </div>

                    <div class="btnAdminThird">
                        <a title="Générer un code QR" target="qrCode"
                           href="https://fr.qr-code-generator.com/phpqrcode/getCode.php?cht=qr&chl=<?php echo $exposant->hashId; ?>&chs=290x290&choe=UTF-8&chld=L|0"><i
                                    class="fa fa-qrcode"></i></a>
                    </div>
                </div>
            </td>
        </tr>
	<?php endforeach; ?>
</table>

<?php include 'footerAdmin.php'; ?>

