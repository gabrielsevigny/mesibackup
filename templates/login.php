<?php 
    /*
     * Paramètres attendus
     * 
     * $data['errorMessage']
     * 
     */
?>

<?php include 'header.php'; ?>
    <style>
        .banner__top-nav {
            display: none;
        }
    </style>
<div class="mainTitle">
    <h1>Gagnez un voyage à Montréal !<br><span class="english">Win a trip to Montréal !</span></h1>
</div>
<?php if (!empty($data['errorMessage'])): ?>
    
    <div class="errorMessage msg msg--alert">
        <p>Veuillez remplir correctement tous les champs</p>
        <p>Please fill in all the fields correctly</p>
    </div>

<?php endif; ?>

<form action="<?php Config::appURL ?>login/auth" method="post">
    <input type="text" name="txtNom" placeholder="Nom complet / Full Name (required)" required><br>
    <input type="email" name="txtEmail" placeholder="Courriel / Email (required)" required><br>
    <input type="tel" name="txtTelephone" placeholder="Téléphone / Phone"><br>
    <input type="text" name="txtCompagnie" placeholder="Compagnie / Company (required)" required><br>
    <input type="submit" value="Continuer / Continue" class="btn btn-green">
</form>

<?php include 'footer.php'; ?>