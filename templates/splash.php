<?php include 'header.php'; ?>
<script>
    $(function () {

        var splashLoop = 0;
        window.setInterval(function () {
            console.log(splashLoop);
            if (splashLoop == 7) {
                goHome();
            } else {
                $('#splashLoading span').eq(splashLoop).addClass('active');
                splashLoop++;
            }
        }, 1000);

    });

    function goHome() {
        $('body').fadeOut(600, function () {
            window.location.replace("/home");
            return false;
        });
    }

</script>

<style>
    .banner__top {
        display: none;
    }

    .main--footer {
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
        background-color: white;
    }
</style>

</head>
<body>
<div class="splashpage" style="background-image: url('/assets/img/backsplashpage.png');">
    <img class="splashpage-header" src="/assets/img/headersplash.png" alt="Bio2018">
    <h1>Bienvenue<br><span class="english">Welcome</span></h1>

    <noscript>
        <h2>Javascript doit être activé sur votre appareil pour pouvoir utiliser cette application.<br>
            <span class="english">Javascript must be enabled on your device in order to use this application.</span>
        </h2>
    </noscript>
    <div id="splashLoading">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>

    <a class="btn btn-green waves-effect waves-light" href="#" onclick="return goHome();">Continuer<br>Continue</a>

</div>
<?php include 'footer.php'; ?>
