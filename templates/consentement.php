<?php include 'header.php'; ?>
<style>
    .banner__top-nav {
        display: none;
    }

    hr {
        display: block;
        margin: 30px 0;
    }

    .btn {
        margin-top: 30px;
    }
</style>
<h1>Termes et conditions</h1>
<p>En participant à ce concours, vous autorisez Export Québec et Tourisme Montréal à utiliser vos informations à des
    fins de publicité.</p>
<hr>
<h1 class="english">Terms And Conditions</h1>
<p class="greenTypo">By participating in this contest, you allow Tourism Montreal and Export Quebec to use your name and
    image for
    publicity purposes.</p>
<p>
    <a class="btn btn-green waves-light waves-effect" href="/login/accepter">J'accepte<br>I agree</a></p>
<?php include 'footer.php'; ?>
