<?php
/*
 * Affiche la liste de tous les exposants
 * 
 * paramètres attentdus:
 * $data['exposants'] array d'objet Exposants
 */
?>
<?php include 'header.php'; ?>

    <div class="mainTitle">
        <h1>Tous les exposants québécois<br>
            <span class="english">All Quebec Exhibitors</span></h1>
    </div>

    <div class="twoBtnHalf">
        <div class="btnHalf">
            <a class="btn btn-green waves-light waves-effect" href="/rapports/exposants">Tous les exposants québécois<br><br>All Quebec Exhibitors</a>
        </div>
        <div class="btnHalf">
            <a class="btn btn-gray waves-effect waves-light" href="/rapports/mesvisites">Liste de mes rencontres<br><br>List of my meetings</a>
        </div>
    </div>

    <ul class="liste--exposants liste--exposants-front">
		<?php foreach ( $data['exposants'] as $exposant ): ?>
            <li>
                <?php echo $exposant->prenom; ?><?php echo $exposant->nom; ?><br>
                <strong><?php echo $exposant->compagnie; ?><br></strong>
                <a href="mailto:<?php echo $exposant->email; ?>"><?php echo $exposant->email; ?></a><br>
                <?php if (!empty($exposant->telephone)): ?>
                    <?php echo $exposant->telephone; ?><br>
                <?php endif; ?>

                <?php if($exposant->pdfPath): ?>
                 <a class="btnTxt waves-effect waves-light" href='<?php echo $exposant->pdfPath; ?>'>Plus d'info<br>More information</a>
                <?php endif; ?>

            </li>
		<?php endforeach; ?>
    </ul>


<?php include 'footer.php'; ?>