<html>
<head>
    <title><?php echo Config::appTitle; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/normalize.css">
    <link rel="stylesheet" href="/assets/styles.css">
    <script src="/assets/jQuery.js" type="text/javascript"></script>
    <script src="/assets/qr.js" type="text/javascript"></script>
</head>
<body>
<div id="bodyWrapper" class="bodyWrapperDisable">

    <header class="banner__top">
        <div class="banner__top-nav">
            <button class="backNav waves-effect waves-light" onclick="window.location.href='/home'">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </button>

            <div class="siteTitle siteTitle-small">Bonjour / Hello</div>
          <!--  <button class="closeAdmin backNav waves-effect waves-light" onclick="window.location.href='/login/logout'">
                <i class="fa fa-power-off" aria-hidden="true"></i>
            </button>-->

        </div>
        <img src="/assets/img/bannertop.png" alt="">
    </header>
	<?php // if ( $Session->isLoggued() ): ?>
	<?php // echo $Session->User->nom; ?>
	<?php // endif; ?>
    <div class="bodyWrapper-content">