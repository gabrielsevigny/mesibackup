<?php include 'header.php'; ?>
    <div class="mainTitle">
        <h1>Trouvez et scannez<br>un partenaire d'affaires<br>
            <span class="english">Find and scan<br>a business partner</span></h1>
    </div>

    <label class="qrCode">
        <!--<i class="fa fa-qrcode"></i>-->
        <span class="txt">
            Cliquez ici<br>pour scanner<div class="english">Click here<br>to scan</div>
        </span>
        <span class="hautDroit"></span>
        <span class="hautGauche"></span>
        <span class="basDroit"></span>
        <span class="basGauche"></span>
        <input type=file accept="image/*" capture="environment" onchange="openQRCamera(this);"
               value="Scanner le code maintenant">
    </label>
    <p><a class="btn btn-green waves-effect waves-light" href="/rapports/exposants">Répertoire des exposants<br>Exhibitors
            directory<span class="blueTypo plusPetit">Cliquez ici / Click here</span></a></p>

    <div class="participantCounter">
        <div class="participantCounter-title"><span>Nombre de rencontres<br><i>Meetings to date</i></span></div>
        <div class="participantCounter-number"><a href="/rapports/mesvisites"><?php echo $Session->User->nbVisite(); ?></a></div>
    </div>
<?php include 'footer.php'; ?>


<?php
/* Ajout d'exposant test
 
    $e = new Exposant();
    $e->prenom='Jhon';
    $e->nom='Doe';
    $e->email='jhon.doe@abccorp.com';
    $e->compagnie='ABC Corp';
    $e->pdfPath='abccorp.pdf';
    $e->save();
    unset($e);
    
    $e = new Exposant();
    $e->prenom='Blow';
    $e->nom='Joe';
    $e->email='blow.joe@xyinc.com';
    $e->compagnie='XY inc.';
    $e->pdfPath='xyinc.pdf';
    $e->save();
    unset($e);
    
    $e = new Exposant();
    $e->prenom='Billy';
    $e->nom='Bob';
    $e->email='billy.bob@billinternational.com';
    $e->compagnie='Bill International';
    $e->pdfPath='bill-international.pdf';
    $e->save();
    unset($e);
 
// */