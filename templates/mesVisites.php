<?php
/*
 * Affiche la liste des exposants qui ont été scannés par l'utilisateur
 */
?>

<?php include 'header.php'; ?>

    <div class="mainTitle">
        <h1>Mes rencontres<br>
        <span class="english">My meetings</span></h1>
    </div>

    <div class="twoBtnHalf">
        <div class="btnHalf">
            <a class="btn btn-green waves-effect waves-light" href="/rapports/mesvisites">Liste de mes rencontres<br><br>List of my meetings</a>
        </div>
        <div class="btnHalf">
            <a class="btn btn-gray waves-light waves-effect" href="/rapports/exposants">Tous les exposants québécois<br><br>All Quebec exhibitors</a>
        </div>
    </div>

<?php if ( count( $Session->User->listExposants() ) ): ?>

    <ul class="liste--exposants liste--exposants-front">
		<?php foreach ( $Session->User->listExposants() as $exposant ): ?>
            <li>
                <?php echo $exposant->prenom; ?><?php echo $exposant->nom; ?><br>
                <strong> <?php echo $exposant->compagnie; ?><br></strong>
                <a href="mailto:<?php echo $exposant->email; ?>"><?php echo $exposant->email; ?></a><br>
                <?php echo $exposant->telephone; ?>
	            <?php if($exposant->pdfPath): ?>
                    <a class="btnTxt waves-effect waves-light" href='<?php echo $exposant->pdfPath; ?>'>Plus d'info<br>More information</a>
	            <?php endif; ?>
            </li>
		<?php endforeach; ?>
    </ul>

<?php else: ?>

    <div class="msg msg--warning">
        <p>Vous n'avez pas encore visité d'exposant.<br>
            You have not visited any exhibitors yet.</p>
    </div>

<?php endif; ?>

<?php include 'footer.php'; ?>